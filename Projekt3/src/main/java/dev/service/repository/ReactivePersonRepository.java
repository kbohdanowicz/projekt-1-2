package dev.service.repository;

import dev.domain.Person;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;

public interface ReactivePersonRepository extends ReactiveMongoRepository<Person, Integer> {

}
