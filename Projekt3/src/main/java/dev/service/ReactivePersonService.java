package dev.service;

import dev.domain.Person;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface ReactivePersonService {

    void save(Person person);

    Mono<Person> findById(int id);

    Flux<Person> findAll();

    Mono<Person> update(Person person);

    Mono<Void> delete(int id);
}

