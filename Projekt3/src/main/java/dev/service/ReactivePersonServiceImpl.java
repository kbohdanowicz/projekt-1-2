package dev.service;

import dev.domain.Person;
import dev.service.repository.ReactivePersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service("reactivePersonService")
public class ReactivePersonServiceImpl implements ReactivePersonService{

    @Autowired
    ReactivePersonRepository rpr;

    public void save(Person person) {
        rpr.save(person).subscribe();
    }

    public Mono<Person> findById(int id) {
        return rpr.findById(id);
    }

    public Flux<Person> findAll() {
        return rpr.findAll();
    }

    public Mono<Person> update(Person person) {
        return rpr.save(person);
    }

    public Mono<Void> delete(int id) {
        return rpr.deleteById(id);
    }
}
