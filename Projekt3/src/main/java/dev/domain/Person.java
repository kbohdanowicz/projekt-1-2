package dev.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Person {

    @Id
    private int id;

    private String firstName;

    private String lastName;

    private String nick;

    private String password;

    private String email;

    private boolean admin;
}
