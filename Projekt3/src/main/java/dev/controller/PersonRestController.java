package dev.controller;

import dev.domain.Person;
import dev.service.ReactivePersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
public class PersonRestController {

    private ReactivePersonService rps;

    @Autowired
    public void setReactivePersonService(@Qualifier("reactivePersonService") ReactivePersonService rps) {
        this.rps = rps;
    }

    @PostMapping("/api/post")
    public void save(@RequestBody Person person) {
        rps.save(person);
    }

    @DeleteMapping("/api/delete/{id}")
    public void delete(@PathVariable("id") Integer id) {
        rps.delete(id).subscribe();
    }

    @GetMapping("/api/get")
    public Flux<Person> findAll() {
        return rps.findAll();
    }

    @GetMapping("/api/get/{id}")
    public ResponseEntity<Mono<Person>> findById(@PathVariable("id") Integer id) {
        Mono<Person> person = rps.findById(id);
        HttpStatus status = person != null ? HttpStatus.OK : HttpStatus.NOT_FOUND;
        return new ResponseEntity<>(person, status);
    }

    @PutMapping("/api/put")
    public Mono<Person> update(@RequestBody Person person) {
        return rps.update(person);
    }
}
