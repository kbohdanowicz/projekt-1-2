package dev.project;

import dev.project.domain.Person;
import dev.project.service.PersonInMemoryService;
import dev.project.service.PersonManager;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@Service
class PersonInMemoryServiceTest {

    private PersonManager pm;

    @BeforeEach
    void setUp() {
        pm = new PersonInMemoryService();
        pm.setPersons(new ArrayList<>());
        pm.add(new Person(1, "FirstName", "LastName", "Email", "Male", "CC type", "123456789011121"));
    }

    @AfterEach
    void tearDown(){
        pm.remove(1);
    }

    @Test
    void addPerson() {
        pm.remove(1);
        pm.add(new Person(1, "FirstName", "LastName", "Email", "Male", "CC type", "123456789011121"));
        List<Person> personList = pm.getAllPersons();
        Person person = personList.get(personList.size() - 1);
        assertTrue(person != null && person.getId() == 1);
    }

    @Test
    void removePerson() {
        List<Person> personList = pm.getAllPersons();
        Person person = personList.get(personList.size() - 1);
        pm.remove(person.getId());
        assertNull(pm.findById(person.getId()));
    }

    @Test
    void editPerson() {
        List<Person> personList = pm.getAllPersons();
        Person person = personList.get(personList.size()-1);
        Person editedPerson = new Person(1,"EditedName", "LastName", "Email", "Male","CC type","123456789011121");
        pm.edit(editedPerson);
        assertEquals("EditedName", pm.findById(person.getId()).getFirstName());
    }

    @Test
    void findById() {
        List<Person> personList = pm.getAllPersons();
        Person person = personList.get(personList.size()-1);
        assertNotNull(pm.findById(person.getId()));
    }

    @Test
    void getAllPersons() {
        assertFalse(pm.getAllPersons().isEmpty());
    }

    @Test
    void setPersons() {
        pm.setPersons(new ArrayList<>());
        List<Person> personList = pm.getAllPersons();
        assertNotNull(personList);
    }
}