package dev.project.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.opencsv.CSVWriter;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;
import dev.project.domain.Person;
import dev.project.service.PersonManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;
import java.io.IOException;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Paths;

@Controller
public class PersonController {

    private PersonManager pm;

    @Autowired
    public PersonController(PersonManager pm) {
        this.pm = pm;
    }

    @GetMapping("/person")
    public String home(Model model){
        model.addAttribute("persons", pm.getAllPersons());
        return "person-all";
    }

    @GetMapping("/person/")
    public String home2(Model model){
        model.addAttribute("persons", pm.getAllPersons());
        return "person-all";
    }

    @GetMapping("/person/add")
    public String newPerson(Model model){
        model.addAttribute("person", new Person());
        return "person-add";
    }

    @PostMapping("/person/add-error")
    public String addPerson(@Valid Person person, Errors errors) {
        if(errors.hasErrors()){
            return "person-add";
        }
        pm.add(person);
        return "redirect:/person";
    }

    @GetMapping("/person/find")
    public String newFindPerson(Model model){
        model.addAttribute("person", new Person());
        return "person-find";
    }

    @PostMapping("/person/find/result")
    public String findPerson(Person person, Errors errors, Model model) {
        if(errors.hasErrors()){
            return "redirect:/person/find";
        }
        model.addAttribute("person", pm.findById(person.getId()));
        return "person-find-result";
    }

    @GetMapping("/person/delete/{id}")
    public String deletePerson(@PathVariable("id") int id) {
        pm.remove(id);
        return "redirect:/person";
    }

    @GetMapping("/person/edit/{id}")
    public String newEditedPerson(@PathVariable("id") int id, Model model){
        model.addAttribute("person", pm.findById(id));
        return "person-edit";
    }

    @PostMapping("/person/edit")
    public String editPerson(@Valid Person person, Errors errors) {
        if(errors.hasErrors()){
            return "person-edit";
        }
        pm.edit(person);
        return "redirect:/person";
    }

    @GetMapping("/person/export-csv")
    public String exportCsv() throws IOException, CsvDataTypeMismatchException, CsvRequiredFieldEmptyException {
        final String outputPath = "./output-file.csv";
        try (Writer writer = Files.newBufferedWriter(Paths.get(outputPath));) {
            StatefulBeanToCsv<Person> beanToCsv = new StatefulBeanToCsvBuilder(writer)
                    .withQuotechar(CSVWriter.NO_QUOTE_CHARACTER)
                    .build();
            beanToCsv.write(pm.getAllPersons());
        }
        return "person-export-csv";
    }

    @GetMapping("/person/json/{id}")
    public String newPersonJson(@PathVariable("id") int id, Model model) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        String jsonString = mapper.writeValueAsString(pm.findById(id));
        model.addAttribute("personJson", jsonString);
        return "person-json";
    }
}
