package dev.project.domain;

import com.opencsv.bean.CsvBindByName;
import dev.project.validator.Email;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

import javax.validation.constraints.*;

@Component("person")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Person {

    @CsvBindByName(column = "id")
    private int id;

    @NotNull
    @Pattern(regexp = "[A-Z][a-z]+", message = "Invalid first name")
    @CsvBindByName(column = "first_name")
    private String firstName;

    @NotNull
    @Pattern(regexp = "[A-Z][a-z]+", message = "Invalid last name")
    @CsvBindByName(column = "last_name")
    private String lastName;

    @NotNull
    @Email
    @CsvBindByName(column = "email")
    private String email;

    @NotNull
    @Pattern(regexp = "Male|Female", message = "Invalid gender")
    @CsvBindByName(column = "gender")
    private String gender;

    @NotNull
    @Pattern(regexp = "[A-z][A-z]+", message = "Invalid credit card type")
    @CsvBindByName(column = "credit card type")
    private String creditCardType;

    @NotNull
    @Pattern(regexp = "[0-9]{19}|[0-9]{16}|[0-9]{15}|[0-9]{14}", message = "Invalid credit card number")
    @CsvBindByName(column = "credit card number")
    private String creditCardNumber;
}
