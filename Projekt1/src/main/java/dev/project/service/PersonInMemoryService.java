package dev.project.service;

import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;
import com.opencsv.bean.HeaderColumnNameMappingStrategy;
import dev.project.domain.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

@Service
public class PersonInMemoryService implements PersonManager {

    @Autowired
    @Qualifier("importCsv")
    private List<Person> persons;

    public void add(Person person) {
        int newId;
        if (persons.isEmpty())
            newId = 1;
        else
            newId = persons.get(persons.size() - 1).getId() + 1;
        person.setId(newId);
        persons.add(person);
    }

    public void edit(Person newPerson) {
        for (Person oldPerson : persons) {
            if (oldPerson.getId() == (newPerson.getId())) {
                oldPerson.setFirstName(newPerson.getFirstName());
                oldPerson.setLastName(newPerson.getLastName());
                oldPerson.setEmail(newPerson.getEmail());
                oldPerson.setGender(newPerson.getGender());
                oldPerson.setCreditCardType(newPerson.getCreditCardType());
                oldPerson.setCreditCardNumber(newPerson.getCreditCardNumber());
                break;
            }
        }
    }

    @Override
    public void remove(int id) {
        Person toRemove = null;
        for (Person person : persons) {
            if (person.getId() == id) {
                toRemove = person;
                break;
            }
        }
        if (toRemove != null) {
            persons.remove(toRemove);
        }
    }

    @Override
    public Person findById(int id) {
        for (Person person : persons) {
            if (person.getId() == id) {
                return person;
            }
        }
        return null;
    }

    public List<Person> getAllPersons() {
        return persons;
    }

    public void setPersons(List<Person> persons) {
        this.persons = persons;
    }
}
