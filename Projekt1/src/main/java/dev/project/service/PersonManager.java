package dev.project.service;

import dev.project.domain.Person;

import java.util.ArrayList;
import java.util.List;

public interface PersonManager {

    void add(Person person);

    void remove(int id);

    void edit(Person newPerson);

    List<Person> getAllPersons();

    void setPersons(List<Person> persons);

    Person findById(int id);
}
