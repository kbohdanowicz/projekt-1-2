package dev.service;

import dev.domain.Person;

import java.util.List;

public interface PersonService {

    Person findById(int id);

    List<Person> findAll();

    void save(Person person);

    void delete(int id);

    void update(Person oldPerson);

    List<Person> findByNickLike(String nick);

    Person findByNickEquals(String nick);

    Person findByEmailEquals(String email);
}
