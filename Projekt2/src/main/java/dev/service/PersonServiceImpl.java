package dev.service;

import dev.domain.Person;
import dev.service.repository.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PersonServiceImpl implements PersonService{

    @Autowired
    private PersonRepository personRepo;

    public Person findById(int id) {
        if (personRepo.findById(id).isPresent())
            return personRepo.findById(id).get();

        System.out.println("PERSON WITH ID: " + id + " DOES NOT EXIST");
        return null;
    }

    public List<Person> findAll() {
        return personRepo.findAll();
    }

    public void save(Person person) {
        personRepo.save(person);
    }

    public void delete(int id) {
        personRepo.deleteById(id);
    }

    public void update(Person oldPerson) {
        Person newPerson = personRepo.getOne(oldPerson.getId());
        newPerson.setFirstName(oldPerson.getFirstName());
        newPerson.setLastName(oldPerson.getLastName());
        newPerson.setNick(oldPerson.getNick());
        newPerson.setPassword(oldPerson.getPassword());
        newPerson.setEmail(oldPerson.getEmail());
        newPerson.setAdmin(oldPerson.isAdmin());
        personRepo.save(newPerson);
    }

    @Override
    public List<Person> findByNickLike(String nick) {
        if (personRepo.findByNickLike(nick) != null)
            return personRepo.findByNickLike(nick);

        System.out.println("PERSON WITH FIRST NAME LIKE: " + nick + " DOES NOT EXIST");
        return null;
    }

    public Person findByNickEquals(String nick) {
        if (personRepo.findByNickEquals(nick) != null)
            return personRepo.findByNickEquals(nick);

        System.out.println("PERSON WITH NICKNAME: " + nick + " DOES NOT EXIST");
        return null;
    }

    public Person findByEmailEquals(String email) {
        if (personRepo.findByEmailEquals(email) != null)
            return personRepo.findByEmailEquals(email);

        System.out.println("PERSON WITH EMAIL: " + email + " DOES NOT EXIST");
        return null;
    }
}
