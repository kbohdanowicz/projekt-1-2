package dev.service;

import dev.domain.Message;
import dev.domain.Person;

import java.util.List;

public interface MessageService {

    Message findById(int id);

    List<Message> findAll();

    void save(Message message);

    void delete(int id);

    void update(Message oldMessage);

    List<Message> findByContentContainingIgnoreCase(String content);

    List<Message> findByAuthor(Person person);
}
