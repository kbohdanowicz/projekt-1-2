package dev.service;

import dev.domain.Message;
import dev.domain.Person;
import dev.service.repository.MessageRepository;
import dev.service.repository.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MessageServiceImpl implements MessageService {

    @Autowired
    private MessageRepository msgRepo;

    public Message findById(int id) {
        if (msgRepo.findById(id).isPresent())
            return msgRepo.findById(id).get();
        System.out.println("MESSAGE WITH ID: " + id + " DOES NOT EXIST");
        return null;
    }

    public List<Message> findAll() {
        return msgRepo.findAll();
    }

    public void save(Message message) {
        msgRepo.save(message);
    }

    public void delete(int id) {
        msgRepo.deleteById(id);
    }

    public void update(Message oldMessage) {
        Message newMessage = msgRepo.getOne(oldMessage.getId());
        newMessage.setContent(oldMessage.getContent());
        msgRepo.save(newMessage);
    }

    public List<Message> findByContentContainingIgnoreCase(String content) {
        return msgRepo.findByContentContainingIgnoreCase(content);
    }

    public List<Message> findByAuthor(Person person) {
        return msgRepo.findByAuthor(person);
    }
}
