package dev.service.repository;

import dev.domain.Message;
import dev.domain.Person;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.util.List;

@Repository
public interface MessageRepository extends JpaRepository<Message, Integer> {

    List<Message> findByContentContainingIgnoreCase(@NotEmpty @Size(max = 256) String content);

    List<Message> findByAuthor(Person person);
}
