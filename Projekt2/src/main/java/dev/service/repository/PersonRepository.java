package dev.service.repository;

import dev.domain.Person;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.util.List;

@Repository
public interface PersonRepository extends JpaRepository<Person, Integer> {

    @Query(value = "SELECT p FROM Person p WHERE nick LIKE %?1%")
    List<Person> findByNickLike(@NotEmpty @Size(min = 2, max = 32) String nick);

    Person findByNickEquals(@NotEmpty @Size(min = 2, max = 32) String nick);

    Person findByEmailEquals(@Email(message = "Invalid email")
                             @NotBlank(message = "Please fill in this field") String email);
}