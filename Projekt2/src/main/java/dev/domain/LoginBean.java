package dev.domain;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import java.util.Objects;

@Data
@NoArgsConstructor
public class LoginBean {

    @NotBlank
    @Length(min = 2, max = 32)
    private String nick;

    @NotBlank
    @Length(min = 8, max = 32)
    private String password;

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        LoginBean loginBean = (LoginBean) o;
        return Objects.equals(nick, loginBean.nick) &&
                Objects.equals(password, loginBean.password);
    }
}
