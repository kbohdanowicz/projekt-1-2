package dev.domain;

import javax.persistence.*;

import javax.validation.constraints.*;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "person", schema = "public")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Person {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotBlank(message = "Please fill in this field")
    @Length(min = 2, message = "First name must have at least 2 characters")
    private String firstName;

    @NotBlank(message = "Please fill in this field")
    @Length(min = 2, message = "Last name must have at least 2 characters")
    private String lastName;

    @NotBlank(message = "Please fill in this field")
    @Length(min = 2, max = 32, message = "Nickname must have at least 2 and at most 32 characters")
    @Column(length = 32, unique = true)
    private String nick;

    @NotBlank(message = "Please fill in this field")
    @Length(min = 8, max = 32, message = "Password must have at least 8 and at most 32 characters")
    @Column(length = 32)
    private String password;

    @Email(message = "Invalid email")
    @NotBlank(message = "Please fill in this field")
    @Column(unique = true)
    private String email;

    @OneToMany(mappedBy = "author", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Message> messages;

    private boolean admin = false;

    public Person(String firstName, String lastName, String nick, String password, String email, boolean admin) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.nick = nick;
        this.password = password;
        this.email = email;
        this.messages = new ArrayList<>();
        this.admin = admin;
    }

    public void addMessage(Message message) {
        if (message.getCreate_date() == null)
            message.setCreate_date(new Timestamp(new java.util.Date().getTime()));
        message.setAuthor(this);
        messages.add(message);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        Person person = (Person) o;
        return id == person.id &&
                admin == person.admin &&
                Objects.equals(firstName, person.firstName) &&
                Objects.equals(lastName, person.lastName) &&
                Objects.equals(nick, person.nick) &&
                Objects.equals(password, person.password) &&
                Objects.equals(email, person.email);
    }

    @Override
    public String toString() {
        return nick;
    }
}
