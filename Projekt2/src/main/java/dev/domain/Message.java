package dev.domain;

import java.sql.Timestamp;
import java.util.Objects;

import javax.persistence.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import lombok.Data;

import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "message", schema = "public")
@Data
@NoArgsConstructor
public class Message {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotBlank
    @Length(min = 1, max = 256, message = "Invalid message length")
    private String content;

    @DateTimeFormat
    private Timestamp create_date;

    @ManyToOne
    private Person author;

    public Message(String content, Person author) {
        this.content = content;
        create_date = new Timestamp(new java.util.Date().getTime());
        author.addMessage(this);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        Message message = (Message) o;
        return id == message.id &&
                Objects.equals(content, message.content) &&
                Objects.equals(create_date, message.create_date) &&
                Objects.equals(author, message.author);
    }

    @Override
    public String toString() {
        return "Message{" +
                "id=" + id +
                ", content='" + content + '\'' +
                ", create_date=" + create_date +
                //", author=" + author +    //circular dependency
                '}';
    }
}