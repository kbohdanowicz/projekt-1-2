package dev.controller;

import dev.domain.CurrentUser;
import dev.domain.Message;
import dev.domain.Person;
import dev.service.MessageService;
import dev.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;

@Controller
public class HomeController {

    private CurrentUser cu;

    private PersonService ps;

    private MessageService ms;

    @Autowired
    public HomeController(CurrentUser cu, PersonService ps, MessageService ms) {
        this.cu = cu;
        this.ps = ps;
        this.ms = ms;
    }

    @GetMapping("/home")
    public String home(Model model) {
        if (cu.getCurrentUser()!= null) {
            model.addAttribute("admin", cu.getCurrentUser().isAdmin());
            model.addAttribute("newMessage", new Message());
            model.addAttribute("myMessages", ms.findByAuthor(cu.getCurrentUser()));
            model.addAttribute("messages", ms.findAll());
            return "home";
        }
        return "redirect:/";
    }

    @PostMapping("/home")
    public String newmsg(Model model, @Valid final Message message, Errors errors) {
        model.addAttribute("admin", cu.getCurrentUser().isAdmin());
        model.addAttribute("newMessage", new Message());
        model.addAttribute("messages", ms.findAll());
        if (errors.hasErrors()) {
            return "home";
        }
        ps.findById(cu.getCurrentUser().getId()).addMessage(message);
        ms.save(message);
        return "redirect:/home";
    }

    @GetMapping("/home/logout")
    public String logout() {
        cu.setCurrentUser(null);
        return "redirect:/";
    }
}
