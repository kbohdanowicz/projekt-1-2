package dev.controller;

import dev.domain.CurrentUser;
import dev.domain.LoginBean;
import dev.domain.Person;
import dev.service.MessageService;
import dev.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;
import java.util.List;

@Controller
public class AdminController {

    private CurrentUser cu;

    private PersonService ps;

    @Autowired
    public AdminController(CurrentUser cu, PersonService ps) {
        this.cu = cu;
        this.ps = ps;
    }

    @GetMapping("/home/admin")
    public String adminPanel(Model model){
        if (cu.getCurrentUser() != null && cu.getCurrentUser().isAdmin()) {
            model.addAttribute("persons", ps.findAll());
            return "admin-panel";
        }
        return "redirect:/";
    }

    @GetMapping("/home/admin/add-user")
    public String newUser(Model model){
        if (cu.getCurrentUser() != null && cu.getCurrentUser().isAdmin()) {
            model.addAttribute("person", new Person());
            return "add-user";
        }
        return "redirect:/";
    }

    @PostMapping("/home/admin/add-user")
    public String newUserValidation(Model model, @Valid Person person, Errors errors) {
        if (cu.getCurrentUser() != null && cu.getCurrentUser().isAdmin()) {

            Person userInDbNick = ps.findByNickEquals(person.getNick());
            Person userInDbEmail = ps.findByEmailEquals(person.getEmail());

            if (errors.hasErrors() || userInDbNick != null || userInDbEmail != null) {
                if (userInDbNick != null)
                    model.addAttribute("errorNicknameTaken", true);
                if (userInDbEmail != null)
                    model.addAttribute("errorEmailTaken", true);

                return "add-user";
            }
            else {
                System.out.println("User with nickname: " + person.getNick() + ", successfully added to database!");
                ps.save(person);
                return "redirect:/home/admin";
            }
        }
        return "redirect:/";
    }

    @GetMapping("/home/admin/edit-user/{id}")
    public String editUserInit(@PathVariable int id, Model model){
        if (cu.getCurrentUser() != null && cu.getCurrentUser().isAdmin()) {
            model.addAttribute("person", ps.findById(id));
            return "edit-user";
        }
        return "redirect:/";
    }

    @PostMapping("/home/admin/edit-user")
    public String editUserSubmit(Model model, @Valid Person person, Errors errors) {
        if (cu.getCurrentUser() != null && cu.getCurrentUser().isAdmin()) {

            Person userInDbNick = getUserByNickFromDb(person);
            Person userInDbEmail = getUserByEmailFromDb(person);

            if (errors.hasErrors() || userInDbNick != null || userInDbEmail != null) {
                if (userInDbNick != null)
                    model.addAttribute("errorNicknameTaken", true);
                if (userInDbEmail != null)
                    model.addAttribute("errorEmailTaken", true);
                return "edit-user";
            }
            ps.update(person);
            return "redirect:/home/admin";
        }
        return "redirect:/";
    }

    private Person getUserByNickFromDb(Person person) {
        Person userInDb = ps.findByNickEquals(person.getNick());
        if (userInDb != null && !userInDb.equals(ps.findById(person.getId())))
            return userInDb;
        return null;
    }

    private Person getUserByEmailFromDb(Person person) {
        Person userInDb = ps.findByEmailEquals(person.getEmail());
        if (userInDb != null && !userInDb.equals(ps.findById(person.getId())))
            return userInDb;
        return null;
    }

    @GetMapping("/home/admin/delete-user/{id}")
    public String deleteUser(@PathVariable int id) {
        if (cu.getCurrentUser() != null && cu.getCurrentUser().isAdmin()) {
            ps.delete(id);
            if(cu.getCurrentUser().getId() == id)
                cu.setCurrentUser(null);
            return "redirect:/home/admin";
        }
        return "redirect:/";
    }

    @GetMapping("/home/admin/find-user")
    public String findUserInit(Model model){
        if (cu.getCurrentUser() != null && cu.getCurrentUser().isAdmin()) {
            model.addAttribute("loginBean", new LoginBean());
            return "find-user";
        }
        return "redirect:/";
    }

    @PostMapping("/home/admin/find-user")
    public String findUserSubmit(LoginBean loginBean, Model model) {
        if (cu.getCurrentUser() != null && cu.getCurrentUser().isAdmin()) {
            List<Person> foundUsers = ps.findByNickLike(loginBean.getNick());
            model.addAttribute("persons", foundUsers);
            return "find-user-result";
        }
        return "redirect:/";
    }
}
