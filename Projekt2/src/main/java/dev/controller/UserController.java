package dev.controller;

import dev.domain.CurrentUser;
import dev.domain.LoginBean;
import dev.domain.Message;
import dev.domain.Person;
import dev.service.MessageService;
import dev.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;
import java.util.List;

@Controller
public class UserController {

    private CurrentUser cu;

    private PersonService ps;

    private MessageService ms;

    @Autowired
    public UserController(CurrentUser cu, PersonService ps, MessageService ms) {
        this.cu = cu;
        this.ps = ps;
        this.ms = ms;
    }

    @GetMapping("/home/details")
    public String editUserInit(Model model){
        if (cu.getCurrentUser() != null) {
            model.addAttribute("person", ps.findById(cu.getCurrentUser().getId()));
            return "view-details";
        }
        return "redirect:/";
    }

    @PostMapping("/home/details")
    public String editUserSubmit(Model model, @Valid Person person, Errors errors) {
        if (cu.getCurrentUser() != null) {
            Person userInDbNick = getUserByNickFromDb(person);
            Person userInDbEmail = getUserByEmailFromDb(person);

            if (errors.hasErrors() || userInDbNick != null || userInDbEmail != null) {
                if (userInDbNick != null)
                    model.addAttribute("errorNicknameTaken", true);
                if (userInDbEmail != null)
                    model.addAttribute("errorEmailTaken", true);

                return "view-details";
            }
            ps.update(person);
            cu.setCurrentUser(person);
            return "redirect:/home";
        }
        return "redirect:/";
    }

    private Person getUserByNickFromDb(Person person) {
        Person userInDb = ps.findByNickEquals(person.getNick());
        if (userInDb != null && !userInDb.equals(cu.getCurrentUser()))
            return userInDb;
        return null;
    }

    private Person getUserByEmailFromDb(Person person) {
        Person userInDb = ps.findByEmailEquals(person.getEmail());
        if (userInDb != null && !userInDb.equals(cu.getCurrentUser()))
            return userInDb;
        return null;
    }

    @GetMapping("/home/delete")
    public String deleteUser() {
        if (cu.getCurrentUser() != null) {
            ps.delete(cu.getCurrentUser().getId());
            return "redirect:/home/logout";
        }
        return "redirect:/";
    }

    @GetMapping("/home/edit-message/{id}")
    public String editMessageInit(@PathVariable int id, Model model){
        if (cu.getCurrentUser() != null) {
            model.addAttribute("message", ms.findById(id));
            return "edit-message";
        }
        return "redirect:/";
    }

    @PostMapping("/home/edit-message")
    public String editMessageSubmit(@Valid Message message, Errors errors) {
        if (cu.getCurrentUser() != null) {
            if(errors.hasErrors()) {
                return "edit-message";
            }
            ms.update(message);
            return "redirect:/home";
        }
        return "redirect:/";
    }

    @GetMapping("/home/delete-message/{id}")
    public String deleteUser(@PathVariable int id) {
        if (cu.getCurrentUser() != null) {
            ms.delete(id);
            return "redirect:/home";
        }
        return "redirect:/";
    }

    @GetMapping("/home/find-message")
    public String findMessageInit(Model model){
        if (cu.getCurrentUser() != null) {
            model.addAttribute("message", new Message());
            return "find-message";
        }
        return "redirect:/";
    }

    @PostMapping("/home/find-message")
    public String findUserSubmit(Message message, Model model) {
        if (cu.getCurrentUser() != null) {
            List<Message> foundMessages = ms.findByContentContainingIgnoreCase(message.getContent());
            model.addAttribute("messages", foundMessages);
            return "find-message-result";
        }
        return "redirect:/";
    }
}
