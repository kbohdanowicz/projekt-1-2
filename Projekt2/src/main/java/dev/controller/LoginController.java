package dev.controller;

import dev.domain.CurrentUser;
import dev.domain.LoginBean;
import dev.domain.Message;
import dev.domain.Person;
import dev.service.MessageService;
import dev.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;

@Controller
public class LoginController {

    private boolean areRecordsInserted = false;

    private CurrentUser cu;

    private PersonService ps;

    private MessageService ms;

    @Autowired
    public LoginController(CurrentUser cu, PersonService ps, MessageService ms) {
        this.cu = cu;
        this.ps = ps;
        this.ms = ms;

        insertRecords();
    }

    @GetMapping("/")
    public String init(Model model) {
        if (cu.getCurrentUser() != null)
            return "redirect:/home";
        model.addAttribute("loginBean", new LoginBean());
        return "login";
    }

    @GetMapping("/login")
    public String loginInit() {
        return "redirect:/";
    }

    @PostMapping("/login")
    public String loginSubmit(Model model, @Valid LoginBean loginBean, Errors errors) {
        if(errors.hasErrors()) {
            System.out.println("Invalid details");
            model.addAttribute("errorInvalidDetails", true);
            return "login";
        }
        Person userInDB = ps.findByNickEquals(loginBean.getNick());
        if (userInDB != null) {
            if (loginBean.getNick().equals(userInDB.getNick()) &&
                    loginBean.getPassword().equals(userInDB.getPassword())) {
                cu.setCurrentUser(userInDB);
                return "redirect:/home";
            }
            else {
                System.out.println("Nickname or password is invalid");
                model.addAttribute("errorInvalidDetails", true);
                return "login";
            }
        }
        else {
            System.out.println("No such user with nickname: " + loginBean.getNick());
            model.addAttribute("errorInvalidDetails", true);
            return "login";
        }
    }

    @GetMapping("/signup")
    public String signupInit(Model model) {
        model.addAttribute("person", new Person());
        return "signup";
    }

    @PostMapping("/signup")
    public String signupSubmit(Model model, @Valid Person newUser, Errors errors) {

        Person userInDbNick = ps.findByNickEquals(newUser.getNick());
        Person userInDbEmail = ps.findByEmailEquals(newUser.getEmail());

        if (errors.hasErrors() || userInDbNick != null || userInDbEmail != null) {
            if (userInDbNick != null)
                model.addAttribute("errorNicknameTaken", true);
            if (userInDbEmail != null)
                model.addAttribute("errorEmailTaken", true);

            return "signup";
        }
        else {
            System.out.println("User with nickname: " + newUser.getNick() + ", successfully signed in!");
            ps.save(newUser);
            cu.setCurrentUser(newUser);
            return "redirect:/home";
        }
    }

    @GetMapping("/insert")
    public String insert() {
        insertRecords();
        return "redirect:/";
    }

    private void insertRecords() {
        if (!areRecordsInserted) {

            Person user1 = new Person("user1", "user1", "user1", "12345678",
                    "user1@mail.com", false);
            Person user2 = new Person("user2", "user2", "user2", "12345678",
                    "user2@mail.com", false);
            Person user3 = new Person("user3", "user3", "user3", "12345678",
                    "user3@mail.com", false);
            Person user4 = new Person("user4", "user4", "user4", "12345678",
                    "user4@mail.com", false);
            Person admin1 = new Person("xx", "yy", "admin1", "12121212",
                    "admin1@mail.com", true);
            Person admin2 = new Person("xx", "yy", "admin2", "12121212",
                    "admin2@mail.com", true);
            Person admin3 = new Person("xx", "yy", "admin3", "12121212",
                    "admin3@mail.com", true);

            ps.save(user1);
            ps.save(user2);
            ps.save(user3);
            ps.save(user4);
            ps.save(admin1);
            ps.save(admin2);
            ps.save(admin3);

            ms.save(new Message("WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW", user1));
            ms.save(new Message("asfasfaf", user1));
            ms.save(new Message("awf", user1));
            ms.save(new Message("herhe", user2));
            ms.save(new Message("feafef", user2));
            ms.save(new Message("BAabab", user3));
            ms.save(new Message("abc", user3));
            ms.save(new Message("jfowaiuhfuoqwhfoiwqhfjiqowjfipoqfhopqwjhfpqwf", admin1));

            areRecordsInserted = true;
        }
    }
}
